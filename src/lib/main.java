package lib;
import java.sql.*;

public class main {

	public static void main(String[] args) {
		
		String dbUrl = "jdbc:mysql://localhost:3306/userlogin"; 
		// use your port (slide 3)
		String user="root";
		String password="2628orihara";
		//load the driver
		try{ Class.forName("org.gjt.mm.mysql.Driver"); }
		catch(ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println("Error loading the driver!\n");
			}
		try{  //create a connection using the driver, try to do this only once per application for better efficiency.
			Connection c=DriverManager.getConnection(dbUrl, user, password);
			Statement s = c.createStatement();
			//create a query
			ResultSet r = s.executeQuery(" SELECT number,name FROM myTable");
			//print everything out in the console
			System.out.println("number | name");
			System.out.println("-----------------");
			while (r.next()){ //the individual row values can be identified by column name or by column index
				System.out.println(r.getInt("number")+" | "+r.getString(2) );}        
			r.close();        
			s.close();
			}
		catch(SQLException e) {
			e.printStackTrace();}
	}
}// use this code in whatever java application you wish: java app, applet, servlet, web service, etc. And YES , you DO have to change the queries and tables to make it do what YOU want it to do.
